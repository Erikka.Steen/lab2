package INF101.lab2;

import java.util.List;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;


public class Fridge implements IFridge{

    // maks antall på innholdet
    int max_size = 20;

    // Arrayliste med tingene i kjøleskapet
    List<FridgeItem> fridge = new ArrayList<>();


    public int totalSize() {
        return max_size;
    }
    
    public int nItemsInFridge() {
        return fridge.size();
    }

    public boolean placeIn(FridgeItem item) {
        fridge.add(item);
        if (fridge.size() > 20) {
            System.out.print("Too many items in fridge!");
            return false;
        }
        return true;
    }

    public void takeOut(FridgeItem item) {
        if (fridge.remove(item)) {
        }
        else {
            throw new NoSuchElementException();
        }
        
    }


    public void emptyFridge() {
        fridge.clear();
        return;
        }
    
    


    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> foodNotExpired = new ArrayList<>();
        for (FridgeItem item: fridge) {
            if (item.hasExpired() == false) {
                foodNotExpired.add(item);
            }

        }
        fridge.removeAll(foodNotExpired);
        return fridge;
    }

    
}
